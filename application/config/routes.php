<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = 'page/view/home';
$route['404_override'] = '';


//$route['^it/art-and-projects$'] = "art/index";
$route['^(it|fr|en)/art-and-projects$'] = "art/index";
$route['^(it|fr|en)/art-and-projects/(.+)$'] = "art/view/$2";

$route['^(it|fr|en)/work-and-collaborations$'] = "work/index";
$route['^(it|fr|en)/work-and-collaborations/(.+)$'] = "work/view/$2";

//$route['^(it|fr|en)/portfolio$'] = "exhibitions/index";
$route['^(it|fr|en)/portfolio/(.+)$'] = "portfolio/index/$2";

// example: '/en/about' -> uses controller 'about'
$route['^it/(.+)$'] = "page/view/$1";
$route['^fr/(.+)$'] = "page/view/$1";
$route['^en/(.+)$'] = "page/view/$1";
 
// '/en' and '/fr' -> use default controller
$route['^it$'] = $route['default_controller'];
$route['^fr$'] = $route['default_controller'];
$route['^en$'] = $route['default_controller'];

$route['en/about/send'] = 'contacts/send';

/* End of file routes.php */
/* Location: ./application/config/routes.php */