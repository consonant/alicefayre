<?php
// application config

$config['appname']	= 'Alice Fayre';
$config['appurl']	= 'http://alicefayre.com';
$config['apptitle'] = 'Alice Fayre Make up Artist and Hair Stylist';

//$config['contactmail']	= 'filippo.enea@gmail.com';
$config['contactmail']	= 'fayre.alice@gmail.com';

$config['contactphone']	= '+39 342 1230824';
$config['mks']	= 'MKS Milano';

$config['facebook_link'] = 'https://www.facebook.com/alicepozzamkp';
$config['instagram_link'] = 'https://instagram.com/alicefayre/';
$config['linkedin_link'] = 'https://www.linkedin.com/pub/alice-fayre/98/3aa/6a9';
$config['mks_link'] = 'http://www.mks-milano.com/mks-mi/makeup-artists/alice-fayre/';

$config['menu'] = array(
	'home'			=> '',
	'about' 		=> 'about',
);

$config['menu-portfolio'] = array(
	'fashion'		=> 'portfolio/fashion',
	//'beauty'		=> 'portfolio/beauty',
	//'commercial'	=> 'portfolio/commercial',
	'grooming'		=> 'portfolio/grooming',
	'video'			=> 'video'
);

// PORTFOLIO
$config['portfolio'] = array(
	'fashion' => array(
		'love-and-nails-2014' => array(	
			'client'	=> 'Love &amp; Nails',
			'name'		=> '2014',
			'date'		=> '2014-07',
			'gallery_n'	=> 3,
			'tags'		=> 'Make-up', 
			'note'		=> FALSE
		),
		'the-cube-2-artistic-essences' => array(
			'client'	=> 'The Cube n.2',
			'name'		=> 'Artistic Essences',
			'date'		=> '2014-05',
			'gallery_n'	=> 6,
			'tags'		=> 'Make-up, Art Direction', 
			'note'		=> 'Photography: <br><strong>Andrea Benedetti</strong>',
		),
		/******************
		'trucco-e-bellezza-summer' => array(
			'client'	=> '(Stand-By) Trucco &amp; Bellezza',
			'name'		=> 'Summer',
			'date'		=> '2013-12',
			'gallery_n'	=> 4,
			'tags'		=> 'Make-up', 
			'note'		=> 'Photography:<br><strong>Matteo Macchiavello</strong>'
		),
		/******************/
		'20-20-wallpapers' => array(
			'client'	=> '20/20',
			'name'		=> 'Wallpapers',
			'date'		=> '2015-01',
			'gallery_n'	=> 9,
			'tags'		=> 'Make-up, Hair', 
			'note'		=> 'Photography:<br><strong>Chiara Romagnoli</strong>'
		),
		'20-20-framings' => array(
			'client'	=> '20/20',
			'name'		=> 'Framings',
			'date'		=> '2014-03',
			'gallery_n'	=> 6,
			'tags'		=> 'Make-up, Hair', 
			'note'		=> 'Photography:<br><strong>Francesco Bertola</strong>'
		),
		'alla-carta-6' => array(
			'client'	=> 'Alla carta',
			'name'		=> '#6',
			'date'		=> '2015-04',
			'gallery_n'	=> 12,
			'tags'		=> 'Make-up', 
			'note'		=> 'Photography:<br><strong>Yana De Nicola</strong>'
		),
		'bite-magazine-shapeshifter' => array(
			'client'	=> 'Bite Magazine',
			'name'		=> 'Shapeshifter',
			'date'		=> '2014-08',
			'gallery_n'	=> 4,
			'tags'		=> 'Make-up, Hair', 
			'note'		=> 'Photography:<br><strong>Donald Gjoka</strong>'
		),
		'd-repubblica-july-2014-1' => array(
			'client'	=> 'D Repubblica',
			'name'		=> 'July 2014',
			'date'		=> '2014-07',
			'gallery_n'	=> 6,
			'tags'		=> 'Make-up', 
			'note'		=> 'Photography:<br><strong>Matteo Cherubino</strong>'
		),
		'd-repubblica-july-2014-2' => array(
			'client'	=> 'D Repubblica',
			'name'		=> 'July 2014',
			'date'		=> '2014-07',
			'gallery_n'	=> 6,
			'tags'		=> 'Make-up', 
			'note'		=> 'Photography:<br><strong>Matteo Cherubino</strong>'
		),
		'd-repubblica-july-2014-3' => array(
			'client'	=> 'D Repubblica',
			'name'		=> 'July 2014',
			'date'		=> '2014-07',
			'gallery_n'	=> 6,
			'tags'		=> 'Make-up', 
			'note'		=> 'Photography:<br><strong>Matteo Cherubino</strong>'
		),
		'design-scene-february-2015' => array(
			'client'	=> 'Design Scene',
			'name'		=> 'February 2015',
			'date'		=> '2015-02',
			'gallery_n'	=> 8,
			'tags'		=> 'Make-up', 
			'note'		=> 'Photography:<br><strong>Fabrizio Scarpa</strong>'
		),
		'drew-alien' => array(
			'client'	=> 'Drew',
			'name'		=> 'Alien',
			'date'		=> '2014-12',
			'gallery_n'	=> 10,
			'tags'		=> 'Make-up', 
			'note'		=> 'Photography:<br><strong>Marco Castellani &amp; Ren&egrave;e Liszkal</strong>'
		),
		'monsters-management' => array(
			'client'	=> 'Monsters Management',
			'name'		=> FALSE,
			'date'		=> '2014-07',
			'gallery_n'	=> 11,
			'tags'		=> 'Make-up, Hair', 
			'note'		=> 'Photography:<br><strong>Cristiano Miretti</strong>'
		),
		'mp-management-simple' => array(
			'client'	=> 'MP Management',
			'name'		=> 'Simple',
			'date'		=> '2015-05',
			'gallery_n'	=> 6,
			'tags'		=> 'Make-up', 
			'note'		=> 'Photography:<br><strong>Gaultier Pellegrin</strong>'
		),
		'please-magazine-match-your-outfit-to-your-nokia' => array(
			'client'	=> 'Please Magazine',
			'name'		=> 'Match your outfit to your Nokia!',
			'date'		=> '2015-03',
			'gallery_n'	=> 3,
			'tags'		=> 'Make-up', 
			'note'		=> 'Photography:<br><strong>Erica Fava</strong>'
		),
		'pop-management' => array(
			'client'	=> 'Pop Management',
			'name'		=> FALSE,
			'date'		=> '2014-07',
			'gallery_n'	=> 6,
			'tags'		=> 'Make-up, Hair', 
			'note'		=> 'Photography:<br><strong>Cristiano Miretti</strong>'
		),
		'rough-magazine-bionic-heart' => array(
			'client'	=> 'Rough Magazine',
			'name'		=> 'Bionic heart',
			'date'		=> '2015-05',
			'gallery_n'	=> 12,
			'tags'		=> 'Make-up, Hair', 
			'note'		=> 'Photography:<br><strong>Elis Jolie</strong>'
		),
		/******************
		'borealis-magazine-syncretism' => array(
			'client'	=> '(Stand-By) Borealis Magazine',
			'name'		=> 'Syncretism',
			'date'		=> FALSE,
			'gallery_n'	=> 5,
			'tags'		=> FALSE, 
			'note'		=> FALSE
		),
		/******************/
		'theone2swatch-shawty-like-a-melody' => array(
			'client'	=> 'Theone2swatch',
			'name'		=> 'Shawty like a melody',
			'date'		=> '2014-06',
			'gallery_n'	=> 6,
			'tags'		=> 'Make-up', 
			'note'		=> 'Photography:<br><strong>Daniele Botallo</strong>'
		),
		'wu-magazine-42-six-ways-to-say-sweatshirt' => array(
			'client'	=> 'WU Magazine n.42',
			'name'		=> 'Six ways to say sweatshirt',
			'date'		=> '2013-11',
			'gallery_n'	=> 7,
			'tags'		=> 'Make-up', 
			'note'		=> 'Photography:<br><strong>Matteo Cherubino</strong>'
		),
		'wu-magazine-52-vinyl-girl' => array(
			'client'	=> 'WU Magazine n.52',
			'name'		=> 'Vinyl Girl',
			'date'		=> '2014-11',
			'gallery_n'	=> 7,
			'tags'		=> 'Make-up', 
			'note'		=> 'Photography:<br><strong>Chiara Romagnoli</strong>'
		),
		'wu-magazine-53-girls-in-town' => array(
			'client'	=> 'WU Magazine n.53',
			'name'		=> 'Girls in town',
			'date'		=> '2014-12',
			'gallery_n'	=> 6,
			'tags'		=> 'Make-up', 
			'note'		=> 'Photography:<br><strong>Chiara Romagnoli</strong>'
		),
	),
	'grooming' => array(
		'class-of-its-own-issue-05' => array(	
			'client'	=> 'Class of its own',
			'name'		=> 'Issue 05',
			'date'		=> '2014',
			'gallery_n'	=> 9,
			'tags'		=> 'grooming', 
			'note'		=> 'Photography:<br><strong>Francesco Bertola</strong>'
		),
		'g-shock-fw-2014' => array(	
			'client'	=> 'G-Shock',
			'name'		=> 'FW 2014',
			'date'		=> '2014-09',
			'gallery_n'	=> 9,
			'tags'		=> 'grooming', 
			'note'		=> 'Photography:<br><strong>Francesco Bertola</strong>'
		),
		'fucking-young-magazine-marc' => array(	
			'client'	=> 'Fucking Young Magazine',
			'name'		=> 'Marc',
			'date'		=> '2014-06',
			'gallery_n'	=> 11,
			'tags'		=> 'grooming', 
			'note'		=> 'Photography:<br><strong>Alessio Beretta</strong>'
		),
		'gq-italia-at-the-barber-shop' => array(	
			'client'	=> 'GQ Italia',
			'name'		=> 'At the barber shop',
			'date'		=> '2014-02',
			'gallery_n'	=> 10,
			'tags'		=> 'make-up', 
			'note'		=> 'Photography:<br><strong>Vanmossevelde+N</strong>'
		),
		'gq-china-rough-path' => array(	
			'client'	=> 'GQ China',
			'name'		=> 'Rough path',
			'date'		=> '2014-10',
			'gallery_n'	=> 9,
			'tags'		=> 'make-up', 
			'note'		=> 'Photography:<br><strong>Vanmossevelde+N</strong>'
		),
		'icon-italia-let-s-get-lost' => array(	
			'client'	=> 'Icon Italia',
			'name'		=> 'Let\'s get lost',
			'date'		=> '2014-04',
			'gallery_n'	=> 10,
			'tags'		=> 'make-up', 
			'note'		=> 'Photography:<br><strong>Stefano Galuzzi</strong>'
		),
		'kaltblut-mag-mexican-madness' => array(	
			'client'	=> 'Kaltblut Mag',
			'name'		=> 'Mexican Madness!',
			'date'		=> '2014-06',
			'gallery_n'	=> 13,
			'tags'		=> 'grooming', 
			'note'		=> 'Photography:<br><strong>Debora Pota</strong>'
		),
		'pal-zileri-lab-2015' => array(	
			'client'	=> 'Pal Zileri',
			'name'		=> 'LAB 2015',
			'date'		=> '2014-06',
			'gallery_n'	=> 14,
			'tags'		=> 'make-up', 
			'note'		=> 'Photography:<br><strong>Johan Sandberg</strong>'
		),
		'pal-zileri-ss15' => array(	
			'client'	=> 'Pal Zileri',
			'name'		=> 'SS15',
			'date'		=> '2014-06',
			'gallery_n'	=> 13,
			'tags'		=> 'make-up', 
			'note'		=> 'Photography:<br><strong>Johan Sandberg</strong>'
		),
		'risbel-mag-a-little-party-never-hurt-nobody' => array(	
			'client'	=> 'Risbel Mag',
			'name'		=> 'A little party never hurt nobody',
			'date'		=> '2014-12',
			'gallery_n'	=> 7,
			'tags'		=> 'make-up', 
			'note'		=> 'Photography:<br><strong>Matteo Felici</strong>'
		),
		'the-cube-the-stickup' => array(	
			'client'	=> 'The Cube',
			'name'		=> 'The Stickup!',
			'date'		=> '2015-04',
			'gallery_n'	=> 9,
			'tags'		=> 'make-up', 
			'note'		=> 'Photography:<br><strong>Andrea Benedetti</strong>'
		),
		'the-cube-the-green-theory' => array(	
			'client'	=> 'The Cube',
			'name'		=> 'The green theory',
			'date'		=> '2014-01',
			'gallery_n'	=> 4,
			'tags'		=> 'make-up', 
			'note'		=> 'Photography:<br><strong>Andrea Benedetti</strong>'
		),
		'unflop-paper-5' => array(	
			'client'	=> 'Unflop Paper',
			'name'		=> '#5',
			'date'		=> '2013-09',
			'gallery_n'	=> 33,
			'tags'		=> 'grooming', 
			'note'		=> 'Photography:<br><strong>Matt Lambert</strong>'
		),
		'vogue-italia-july-2013' => array(	
			'client'	=> 'Vogue Italia',
			'name'		=> 'July 2013',
			'date'		=> '2013-06',
			'gallery_n'	=> 8,
			'tags'		=> 'grooming', 
			'note'		=> 'Photography:<br><strong>Paolo Santambrogio</strong>'
		),
		'wu-magazine-44-something-4-the-weekend' => array(	
			'client'	=> 'WU Magazine n.44',
			'name'		=> 'Something 4 the weekend',
			'date'		=> '2014-01',
			'gallery_n'	=> 8,
			'tags'		=> 'grooming', 
			'note'		=> 'Photography:<br><strong>Francesco Bertola</strong>'
		),
		'wu-magazine-47-superpop' => array(	
			'client'	=> 'WU Magazine n.47',
			'name'		=> 'Superpop',
			'date'		=> '2014-04',
			'gallery_n'	=> 6,
			'tags'		=> 'grooming', 
			'note'		=> 'Photography:<br><strong>Francesco Bertola</strong>'
		),
	),
);

// VIDEO
$config['video'] = array(
	'pal-zileri-ss15-video' => array(	
			'name'	=> 'Pal Zileri SS15',
			'videoid'	=> 118652696,
	),
	'pal-zileri-lab-2015-video'=> array(	
			'name'	=> 'Pal Zileri Lab 2015',
			'videoid'	=> 111817021,
	),
	'risbel-video'=> array(	
			'name'	=> 'Risbel',
			'videoid'	=> 109710847,
	),
	'syncretism-video' => array(	
			'name'	=> 'Syncretism',
			'videoid'	=> 108811591,
	),
);