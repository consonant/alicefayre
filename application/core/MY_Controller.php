<?php
/**
 * APP_Controller
 */
class MY_Controller extends CI_Controller {
	protected $view_data;
	protected $has_layout = TRUE;
	
	/*
	 * layout / autoview functionality
	 */
	public $platform = 'desktop';
	protected $layout_view = 'application';
	protected $content_view = '';

	// --------------------------------------------------------------------
	/**
	 * Costruttore
	 */
	function __construct() {
		parent::__construct();

		// user agent library
		$this->load->library('user_agent');

		// is mobile?
		if($this->agent->is_mobile())
		{
			$this->platform = 'mobile';
		}

		// forced platform
		if(config_item('platform') && config_item('platform') != 'auto') 
		{
			$this->platform = config_item('platform');
		}

		// session data
		$this->view_data['flash_message'] = $this->session->flashdata('message');
	}


	// --------------------------------------------------------------------
	/**
	 *  _output()
	 *
	 * Post-processing dei dati prima dell'invio al browser
	 * http://codeigniter.com/user_guide/general/controllers.html
	 *
	 */
	function _output() {

		if($this->has_layout) {

			// Il nome della vista da caricare corrisponde a classe/metodo
			$viewname = $this->router->class . '/' . $this->router->method;

			if(isset($this->view_data['view']))
			{
				$viewname = $this->view_data['view'];
			}

			// view per piattaforma
			$view = $this->platform . '/' . $viewname;
			
			if(! file_exists(dirname(__FILE__) . '/../views/' . $this->platform . '/' . $viewname.'.php'))
			{
				// prelevo la vista "desktop"
				// desktop == default/common
				$view = 'desktop/' . $viewname;
			}

			// Precaricamento della vista su view_data['contents']
			$this->view_data['contents'] = $this->load->view($view, $this->view_data, TRUE);

			/* Carica il layout di default dell'applicazione a layout/application.php,
			 * che compone dinamicamente la pagina prelevando le viste salvate nell'array $view_data
			 */
			$layoutview = $this->platform . '/layout/' . $this->layout_view;
			if(! file_exists(dirname(__FILE__) . '/../views/' . $this->platform . '/layout/' . $this->layout_view.'.php'))
			{
				// prelevo il layout "desktop"
				// desktop == default/common
				$layoutview = 'desktop/layout/' . $this->layout_view;
			}
			$this->load->view($layoutview, $this->view_data);

			// Stampa dell'output memorizzato nell'istanza $this->Output
			echo $this->output->get_output();
		}
	}

}
