<?php
function excerpt($string, $n_char = 100)
{
	$str = substr(strip_tags($string), 0, $n_char);
	$str .= '...';
	return $str;
}

function render_element($element, $data = FALSE)
{
	$CI =& get_instance();

	$base_path = APPPATH . 'views/';

	$element_generic_i18n = 'desktop/elements/' . $element . '_' . $CI->lang->lang();
	$element_generic_i18n_base = 'desktop/elements/' . $element . '_en';
	$element_generic = 'desktop/elements/' . $element;
	
	$element_specific_i18n = $CI->platform . '/elements/' . $element . '_' . $CI->lang->lang();
	$element_specific_i18n_base = $CI->platform . '/elements/' . $element . '_en';
	$element_specific = $CI->platform . '/elements/' . $element;

	


	if(file_exists($base_path . $element_specific_i18n . EXT)) $CI->load->view($element_specific_i18n, $data);
	elseif(file_exists($base_path . $element_specific_i18n_base . EXT)) $CI->load->view($element_specific_i18n_base, $data);
	elseif(file_exists($base_path . $element_specific . EXT)) $CI->load->view($element_specific, $data);
	elseif(file_exists($base_path . $element_generic_i18n . EXT)) $CI->load->view($element_generic_i18n, $data);
	elseif(file_exists($base_path . $element_generic_i18n_base . EXT)) $CI->load->view($element_generic_i18n_base, $data);
	elseif(file_exists($base_path . $element_generic . EXT)) $CI->load->view($element_generic, $data);
	/***/
		else echo '
		ERROR no-element: <em>'.$element_specific.'</em>
		or <em>'.$element_generic.'</em>
		';
	/****/

}

function flash($type, $msg)
{
	$CI =& get_instance();
	$messages = $CI->session->flashdata('message');
	$messages[] = array($type => $msg);
	$CI->session->set_flashdata('message', $messages);
}

function slug($title)
{
	return url_title($title, 'dash', TRUE);
}

function print_r2($data)
{
	echo '<div style="border:1px solid red; background:#ffaaaa; padding:20px; margin:10px; max-height:450px; overflow:auto;">';
	echo '<pre>';
	print_r($data);
	echo '</pre>';
	echo '</div>';
}


function format_date($date, $hours = false) {

		$mon = array(
			'january',
			'february',
			'march',
			'april',
			'may',
			'june',
			'july',
			'august',
			'september',
			'october',
			'november',
			'december'
		);

		$CI =& get_instance();
		$lang = $CI->lang->lang();

		$ed = explode('-', $date);
		$howmuch = count($ed);

		if(!isset($ed[1])) $ed[1] = 1;
		if(!isset($ed[2])) $ed[2] = 1;


		//$d = strtotime($d);
		$d = mktime(0, 0, 0, $ed[1], $ed[2], $ed[0]);
		$return = '';

		//$gs = date("w",$d); // Giorno della settimana
		//$return .= $gs.' ';


		$a = $m = $m_str = $g = FALSE;
		$anno_corrente = date('Y');

		if($howmuch >= 1) {
			// year
			$a = date("Y",$d); // Anno
		}

		if($howmuch >= 2) {
			// month
			$m = date("m",$d); // Mese
			$m_str = lang('cal_'.$mon[($m-1)]);
		}

		if($howmuch == 3) {
			$g = date("j",$d); // Giorno	
		}
		

		if($lang == 'en') {
			$return = $m_str.' '.$g;
		}
		else {
			$return = $g.' '.$m_str;
		}

		if($anno_corrente != $a || $howmuch == 1) {
			if($lang == 'en' && $howmuch >= 2) {
				$return .= ',';
			}

			$return .= ' '.$a;
		}

		
		if($hours) {
			$o = date("H:i",$d); // Orario
			$return .= ' at ';
			$return .= $o;
		}

		return $return;
}//function format_date($d)

function format_date_compact($d) {
	$d = strtotime($d);
	$g = date("d",$d); // Giorno
	$m = date("m",$d); // Mese
	$a = date("Y",$d); // Anno
	$o = date("H:i",$d); // Orario

	$return = $g.'/'.$m.'/'.$a.' '.$o;

	return $return;
}


function array_orderby()
{
    $args = func_get_args();
    $data = array_shift($args);
    foreach ($args as $n => $field) {
        if (is_string($field)) {
            $tmp = array();
            foreach ($data as $key => $row)
                $tmp[$key] = $row[$field];
            $args[$n] = $tmp;
            }
    }
    $args[] = &$data;
    call_user_func_array('array_multisort', $args);
    return array_pop($args);
}