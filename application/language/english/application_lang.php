<?php

$lang['app_test'] = 'app test eng';

// payoff
$lang['payoff'] = 'Make up artist and hairstylist based in Milan.';

// menu
$lang['menu.home'] = 'Home';
$lang['menu.about'] = 'About';
$lang['menu.beauty'] = 'Beauty';
$lang['menu.commercial'] = 'Commercial';
$lang['menu.fashion'] = 'Fashion';
$lang['menu.grooming'] = 'Grooming';
$lang['menu.video'] = 'Video';
$lang['menu.contacts'] = 'Contacts';
$lang['menu.portfolio'] = 'Portfolio';

$lang['archive'] = 'Archive';
$lang['view-portfolio'] = 'View Portfolio';

$lang['more-details'] = 'More Details';
$lang['view-collections'] = 'View Collections';

$lang['more-categories'] = 'More from the portfolio';


/*********************
 * SEO
 *********************/

// default / home
$lang['title-home'] = '';
$lang['meta-description-home'] = 'Alice Fayre, Make up Artist and Hair Stylist based in Milan: official website featuring the artist’s portfolio, bio and contacts.';
$lang['meta-keywords-home'] = 'makeup, makeup artist, make up, make up artist, hairstylist, hair stylist, professional makeup, professional make up, wedding make up, wedding makeup, fashion, editorial, fashion shooting, artist, hair, grooming, beauty, art, bridal makeup, bridal make up, wedding hair, bride makeup, hair and makeup artist, make up artists, makeup artistry, hair and makeup, makeup artist website, make up art, makeup art, artistry makeup, hair makeup, artistry make up, hair make up';

// about
$lang['title-about'] = 'About and contacts';
$lang['meta-description-about'] = 'Contact Alice Fayre, Make up Artist and Hair Stylist based in Milan. Also available for weddings, events and as personal consultant.';
$lang['meta-keywords-about'] = 'makeup, makeup artist, make up, make up artist, hairstylist, hair stylist, professional makeup, professional make up, wedding make up, wedding makeup, fashion, editorial, fashion shooting, artist, hair, grooming, beauty, art, bridal makeup, bridal make up, wedding hair, bride makeup, hair and makeup artist, make up artists, makeup artistry, hair and makeup, makeup artist website, make up art, makeup art, artistry makeup, hair makeup, artistry make up, hair make up';

// portfolio-fashion
$lang['title-fashion'] = 'Fashion';
$lang['meta-description-portfolio-fashion'] = 'Alice Fayre, Make up Artist and Hair Stylist based in Milan: portfolio section featuring fashion shootings and editorials.';
$lang['meta-keywords-portfolio-fashion'] = 'makeup, makeup artist, make up, make up artist, hairstylist, hair stylist, professional makeup, professional make up, fashion, editorial, fashion shooting, artist, hair, grooming, beauty, art, hair and makeup artist, make up artists, makeup artistry, hair and makeup, makeup artist website, make up art, makeup art, artistry makeup, hair makeup, artistry make up, hair make up, portfolio, fashion magazine, d repubblica, repubblica, repubblica magazine, wu magazine, the cube, bite magazine, design scene, please magazine, rough magazine, magazine, online magazine, editorial';

// portfolio-grooming
$lang['title-grooming'] = 'Grooming';
$lang['meta-description-portfolio-grooming'] = 'Alice Fayre, Make up Artist and Hair Stylist based in Milan: portfolio section featuring men’s fashion shootings and editorials.';
$lang['meta-keywords-portfolio-grooming'] = 'makeup, makeup artist, make up, make up artist, hairstylist, hair stylist, professional makeup, professional make up, fashion, editorial, fashion shooting, artist, hair, grooming, beauty, art, hair and makeup artist, make up artists, makeup artistry, hair and makeup, makeup artist website, make up art, makeup art, artistry makeup, hair makeup, artistry make up, hair make up, portfolio, fashion magazine, mag, magazine, online magazine, editorial, men, mens fashion, the cube, risbel mag, gq, gq china, gq magazine, g-shock, g shock, fucking young magazine, kaltblut mag, pal zileri, pal zileri ss15, pal zileri lab, icon italia, wu magazine, gq italia, unflop paper, vogue, vogue italia, vogue magazine';

// video
$lang['title-video'] = 'Video';
$lang['meta-description-video'] = 'Alice Fayre, Make up Artist and Hair Stylist based in Milan: video portfolio section.';
$lang['meta-keywords-video'] = 'makeup, makeup artist, make up, make up artist, hairstylist, hair stylist, professional makeup, professional make up, fashion, editorial, fashion shooting, artist, hair, grooming, beauty, art, hair and makeup artist, make up artists, makeup artistry, hair and makeup, makeup artist website, make up art, makeup art, artistry makeup, hair makeup, artistry make up, hair make up, portfolio, fashion magazine, mag, magazine, online magazine, editorial, men, mens fashion, pal zileri, pal zileri ss15, pal zileri lab, pal zileri lab 2015, Syncretism, video, fashion video, campaign, video campagin, brand video, new collection, risbel magazine, risbel mag, creative';