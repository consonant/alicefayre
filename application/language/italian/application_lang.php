<?php

$lang['app_test'] = 'app test ita';

// menu
$lang['menu.home'] = 'Home';
$lang['menu.art-and-projects'] = 'Arte&amp;Progetti';
$lang['menu.work-and-collaborations'] = 'Lavoro&amp;Collaborazioni';
$lang['menu.press'] = 'Stampa';
$lang['menu.exhibitions'] = 'Esposizioni';
$lang['menu.biography'] = 'Biografia';
$lang['menu.contacts'] = 'Contatti';

$lang['archive'] = 'Archivio';

$lang['more-details'] = 'Dettagli';


/*********************
 * SEO
 *********************/

// default / home
$lang['title-home'] = 'Stefano Russo';
$lang['meta-description-home'] = 'Artista e designer poliedrico, utilizza diversi linguaggi, dal disegno alla scultura, dalla fotografia alle installazioni. 
Si dedica alla creazione di strumenti per potenziare i sensi, da vivere o da indossare; ogni oggetto diventa un simbolo, una metafora di riflessione per una continua evoluzione.';
$lang['meta-keywords-home'] = 'stefano russo, home';

// art
$lang['title-art'] = 'Arte&Progetti';
$lang['meta-description-art'] = 'Il lavoro artistico di Stefano Russo si basa sull’essere umano, sul corpo ma soprattutto sui processi percettivi e la sua indagine parte dall’occhio, principale organo sensoriale.';
$lang['meta-keywords-art'] = 'art and projects keywords';

// work
$lang['title-work'] = 'Lavoro&Collaborazioni';
$lang['meta-description-work'] = 'Progetti professionali e collaborazioni con importanti brand nazionali e internazionali';
$lang['meta-keywords-work'] = 'stefano russo, lavoro, collaborazioni, athomie, prada, diesel, swatch';

// exhibitions
$lang['title-exhibitions'] = 'Esposizioni';
$lang['meta-description-exhibitions'] = 'Mostre, esposizioni ed eventi';
$lang['meta-keywords-exhibitions'] = 'stefano russo, esposizioni, salone del mobile, museo';

// biography
$lang['title-biography'] = 'Biografia';
$lang['meta-description-biography'] = 'Stefano Russo nasce in Sicilia nel 1969, vive tra Milano e Parigi. Collabora con realtà che spaziano dai marchi più prestigiosi ad enti e associazioni culturali dello scenario internazionale. 
Ha ricoperto il ruolo di docente presso i laboratori di ricerca del Politecnico di Milano, IED e Domus Academy.';
$lang['meta-keywords-biography'] = 'stefano russo, designer, biografia, arte, design';

// press
$lang['title-press'] = 'Stampa';
$lang['meta-description-press'] = 'Pubblicazioni su Vogue, Vogue Gioiello, Cosmopolitan, JSH, 18 Karati, Corriere della Sera, Glamour, Precious, E & F, Metals Jewellery, Sport & Street, In The World, Donna, Panorama, Ottico Italiano, Musica di Repubblica, Vedere International, Argento, Artigianato, Arte Regalo, Rock Star, Italia Orafa, Arte Regalo, Proposte Top, Optometry, La Clessidra, L’Orafo Italiano, 20/20, Spoon, The Face, Ottagono, Numerò, Vicenza Oro, Wall Paper, Domus, Sole 24h, Repubblica, Corriere, Espoarte, Flashart, Inside, Artribune, Il Giornale';
$lang['meta-keywords-press'] = 'stefano russo, designer, stampa, press, arte, design';

// contacts
$lang['title-contacts'] = 'Contatti studio e laboratorio';
$lang['meta-description-contacts'] = 'Tutti i contatti dell’artista Stefano Russo';
$lang['meta-keywords-contacts'] = 'stefano russo, designer, contatti, studio, arte, design';
