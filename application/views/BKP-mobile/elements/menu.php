<header class="cd-header">
	<a class="cd-primary-nav-trigger" href="#0">
		<span class="cd-menu-icon"></span>
	</a> <!-- cd-primary-nav-trigger -->
	<div class="cd-logo"><a href="<?php echo site_url('home') ?>"><?php echo config_item('appname') ?></a></div>
</header>
<nav>
	<ul class="cd-primary-nav">
		<li class="cd-label">Menu</li>

		<?php foreach(config_item('menu') as $slug) : ?>
			<li class="<?php echo ($section == $slug) ? 'active' : '' ?>">
				<a class="black-text" href="<?php echo site_url($slug) ?>">
					<?php if($slug == 'get-salety') : ?>
						<span class="highlighted"><?php echo lang('menu.'.$slug) ?></span>
					<?php else : ?>
						<?php echo lang('menu.'.$slug) ?>
					<?php endif ?>
				</a>
			</li>
		<?php endforeach ?>

		<li class="cd-label">Follow me</li>
		
		<li class="cd-social"><a href="#0" class="ion-social-facebook"></a></li>
		<li class="cd-social"><a href="#0" class="ion-social-instagram"></a></li>
		<li class="cd-social"><a href="#0" class="ion-social-dribbble"></a></li>
		<li class="cd-social"><a href="#0" class="ion-social-twitter"></a></li>
	</ul>
</nav>