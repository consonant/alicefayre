<?php
$menu = config_item('menu-portfolio');
?>


<div class="middle-h-container full" id="home-hero">
	<div class="text-container">
		<div class="middle-h container">
			<h1>
				MAKE UP ARTIST.
				<br>
				HAIRSTYLIST.
			</h1>
			<a href="#portfolio-categories" class="btn btn-default"><?php echo lang('view-portfolio') ?></a>
		</div>
	</div>
</div>


<div class="clearfix text-center picturebox container-fluid" id="portfolio-categories">
	<div class="row">
		<a id="category-fashion" href="<?php echo site_url($menu['fashion']) ?>">
			<div class="col-sm-4 hidden-xs text middle-h-container">
				<div class="text-container">
					<div class="middle-h">
						<h2 class=""><?php echo lang('menu.fashion') ?>.</h2>
					</div>
				</div>
			</div>
			<div class="col-sm-8 img">
				<h2 class="visible-xs text-sm"><?php echo lang('menu.fashion') ?>.</h2>
			</div>
		</a>
		<?php /***************** ?>
		<a id="category-beauty" href="<?php echo site_url($menu['beauty']) ?>">
			<div class="col-sm-8 img">
				<h2 class="visible-xs text-sm"><?php echo lang('menu.beauty') ?>.</h2>
			</div>
			<div class="col-sm-4 hidden-xs text middle-h-container">
				<div class="text-container">
					<div class="middle-h">
						<h2 class=""><?php echo lang('menu.beauty') ?>.</h2>
					</div>
				</div>
			</div>
		</a>
		
		
		<a id="category-commercial" href="<?php echo site_url($menu['commercial']) ?>">
			<div class="pull-left text middle-h-container">
				<div class="text-container">
					<div class="middle-h">
						<h2 class=""><?php echo lang('menu.commercial') ?>.</h2>
						<!-- <a href="#0" class="btn btn-default"><?php echo lang('view-collections') ?></a> -->
					</div>
				</div>
			</div>
			<div class="pull-right img"></div>
		</a>
		<?php /*****************/ ?>

		<a id="category-grooming" href="<?php echo site_url($menu['grooming']) ?>">
			<div class="col-sm-8 img">
				<h2 class="visible-xs text-sm"><?php echo lang('menu.grooming') ?>.</h2>
			</div>

			<div class="col-sm-4 hidden-xs text middle-h-container">
				<div class="text-container">
					<div class="middle-h">
						<h2 class=""><?php echo lang('menu.grooming') ?>.</h2>
					</div>
				</div>
			</div>
		</a>

		<a id="category-video" href="<?php echo site_url($menu['video']) ?>">
			<div class="col-sm-4 hidden-xs text middle-h-container">
				<div class="text-container">
					<div class="middle-h">
						<h2 class=""><?php echo lang('menu.video') ?>.</h2>
					</div>
				</div>
			</div>
			<div class="col-sm-8 img">
				<h2 class="visible-xs text-sm"><?php echo lang('menu.video') ?>.</h2>
			</div>
		</a>
	</div>

</div>
