<?php
// form data from session
$form = $this->session->userdata('form');
?>

<div class="container text-center">
	<h1><?php echo lang('payoff') ?></h1>
	<h3>For more information please contact me by filling in the form below.</h3>
	<br>
	<h5>Also available for weddings, events and as personal consultant.</h5>
	<br>
	<div name="contactformcontainer" id="contactformcontainer">
		<form class="col-sm-6 col-sm-offset-3" id="contactform" name="contactform" action="<?php echo base_url($this->lang->lang() . '/about/send') ?>" method="post" accept-charset="utf-8">
			<input id="contactname" type="text" name="contactname" class="form-control" placeholder="Name" value="<?php echo $form['contactname'] ?>" required>
			<br>
			<input id="contactemail" type="email" name="contactemail" class="form-control" placeholder="Email" value="<?php echo $form['contactemail'] ?>" required>
			<br>
			<textarea id="contactmessage" rows="9" name="contactmessage" class="form-control" placeholder="Message" required><?php echo $form['contactmessage'] ?></textarea>
			<br>
			<button type="submit" class="btn btn-large btn-default pull-right">Send message</button>
			<br><br><br>
		</form>
	</div>
</div>