<?php
$items = config_item('video');
$menu = config_item('menu-portfolio');
unset($menu['video']);
?>
<div class="container">
	<div class="row">
		<?php foreach($items as $slug => $item) : ?>
			<div class="col-sm-6 portfolio-item">
				<h4 class="list-client"><?php echo $item['name'] ?></h4>
				<div class="video-item">
					<iframe src="https://player.vimeo.com/video/<?php echo($item['videoid']) ?>?title=0&byline=0&portrait=0" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
				</div>
			</div>
		<?php endforeach ?>
	</div>
</div>

<div id="more-categories-container">
	<div class="container">
		<h3><?php echo lang('more-categories') ?></h3>
		<div class="row">
			<?php foreach ($menu as $key => $value) : ?>
				<div class="col-sm-6">
					<a id="id-<?php echo($key) ?>" href="<?php echo site_url($value) ?>">
						<h1 class="categories-link"><?php echo lang('menu.'.$key) ?>.</h1>
					</a>
				</div>
			<?php endforeach ?>
		</div>
	</div>
</div>