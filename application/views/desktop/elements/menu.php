<header class="cd-header">
	<a class="cd-primary-nav-trigger" href="#">
		<span class="cd-menu-icon"></span>
	</a> <!-- cd-primary-nav-trigger -->

	<div class="cd-logo text-center"><a href="<?php echo site_url('/') ?>"><?php echo config_item('appname') ?>.</a></div>

	<div class="cd-social-icon hidden-xs">
		<a href="<?php echo config_item('facebook_link') ?>" class="cd-social ion-social-facebook"></a>
		<a href="<?php echo config_item('instagram_link') ?>" class="cd-social ion-social-instagram"></a>
		<a href="<?php echo config_item('linkedin_link') ?>" class="cd-social ion-social-linkedin"></a>
	</div>
</header>
<nav class="cd-primary-nav">

	<div class="container" id="menu">
		<div class="row">
			<div class="col-md-4">
				
				<p class="cd-label"><?php echo config_item('appname') ?></p>
				<p>
					<?php echo lang('payoff') ?>
				</p>
				
				<ul class="">
					<li class="cd-label">Menu</li>
					<?php foreach(config_item('menu') as $slug => $route) : ?>
						<li class="<?php echo ($section == $slug) ? 'active' : '' ?>">
							<a class="" href="<?php echo site_url($route)?>">
								<?php echo lang('menu.'.$slug) ?>
							</a>
						</li>
					<?php endforeach ?>
				</ul>

			</div>

			<div class="col-md-4">
				<ul class="">
					<li class="cd-label"><?php echo lang('menu.portfolio') ?></li>

					<?php foreach(config_item('menu-portfolio') as $slug => $route) : ?>
						<li class="<?php echo ($section == $slug) ? 'active' : '' ?>">
							<a class="" href="<?php echo site_url($route)?>">
								<?php echo lang('menu.'.$slug) ?>
							</a>
						</li>
					<?php endforeach ?>
				</ul>
			</div>

			<div class="col-md-4">
				<ul class="">
					<li class="cd-label"><?php echo lang('menu.contacts') ?></li>

					<li>
						 <a href="mailto:<?php echo config_item('contactmail') ?>"><?php echo config_item('contactmail') ?></a>
					</li>
					<li>
						 <a href="tel:<?php echo config_item('contactphone') ?>"><?php echo config_item('contactphone') ?></a>
					</li>
					<li>
						 <a href="<?php echo config_item('mks_link') ?>" target="_blank"><?php echo config_item('mks') ?></a>
					</li>
				</ul>

				<ul>
					<li class="cd-label">Social network</li>
					<li>
						<a href="<?php echo config_item('facebook_link') ?>" class="cd-social ion-social-facebook"></a>
						<a href="<?php echo config_item('instagram_link') ?>" class="cd-social ion-social-instagram"></a>
						<a href="<?php echo config_item('linkedin_link') ?>" class="cd-social ion-social-linkedin"></a>
					</li>
				</ul>
			</div>
		</div>
	
	</div>
</nav>