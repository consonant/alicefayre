<footer>
  <div class="container">
    <div class="row">

      <div class="col-sm-6">
        <h2><?php echo config_item('appname') ?>.</h2>
        <p>
         <?php echo lang('payoff') ?>
        </p>
        <address>
          <a href="mailto:<?php echo config_item('contactmail') ?>"><?php echo config_item('contactmail') ?></a>
          <br>
          <a href="tel:<?php echo config_item('contactphone') ?>"><?php echo config_item('contactphone') ?></a>
          <br>
          <a href="<?php echo config_item('mks_link') ?>" target="_blank"><?php echo config_item('mks') ?></a>
        </address>
        <address>
          <a href="<?php echo config_item('facebook_link') ?>" class="cd-social ion-social-facebook"></a>
          <a href="<?php echo config_item('instagram_link') ?>" class="cd-social ion-social-instagram"></a>
          <a href="<?php echo config_item('linkedin_link') ?>" class="cd-social ion-social-linkedin"></a>
        </address>
      </div>

      <div class="col-xs-6 text-right hidden-xs">
        <h5><?php echo lang('menu.portfolio') ?></h5>
        <?php foreach(config_item('menu-portfolio') as $slug => $route) : ?>
            <a class="" href="<?php echo site_url($route)?>">
              <?php echo lang('menu.'.$slug) ?>
            </a>
            <br>
        <?php endforeach ?>

        <h5>Menu</h5>
        <?php foreach(config_item('menu') as $slug => $route) : ?>
            <a href="<?php echo site_url($route) ?>">
              <?php echo lang('menu.'.$slug) ?>
            </a>
            <br>
        <?php endforeach ?>
        <br> 
        <a href="<?php echo site_url('privacy') ?>">Privacy Policy</a>
      </div>
  </div>

  <br><br>
  <p class="text-center">
    Copyright &copy; 2015 <?php echo config_item('appname') ?>. All rights reserved.
  </p>
  
</footer>