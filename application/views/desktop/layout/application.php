<!DOCTYPE html>
<html>
  <head>
    <?php echo render_element('meta') ?>

    <!-- styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/slick.css') ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/slick-theme.css') ?>"/>
    <link href="<?php echo base_url('assets/css/lightgallery.min.css') ?>" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/menu.css') ?>"/>
    <link href="<?php echo base_url('assets/css/application.css?v=10') ?>" rel="stylesheet">

    
</head>

<body id="<?php echo $section ?>-body">

    <?php render_element('menu') ?>

    <?php if($flash_msg = @$this->session->flashdata('message') && $flash_msg != '') : ?>
        <div class="container">
            <p><?php echo $flash_msg ?></p>
        </div>
    <?php endif ?>

    <main id="content">
    	<!-- VIEWCONTENT -->
    	<?php echo $contents ?>
    	<!-- /VIEWCONTENT -->
    </main>
    
	<!-- footer -->
	<?php render_element('footer') ?>
	<!-- /footer -->
	
	<!-- JS -->
	<script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <!-- <script src='<?php echo base_url('assets/js/fastclick.min.js') ?>'></script> -->
    <script src="<?php echo base_url('assets/js/slick.min.js') ?>"></script>
    <!-- <script src="<?php echo base_url('assets/js/jquery.unveil.js') ?>"></script> -->
    <script src="<?php echo base_url('assets/js/lightgallery.min.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/application.js?v=10') ?>"></script>
    <script src="<?php echo base_url('assets/js/desktop.js?v=10') ?>"></script>

    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-550c4b892955cdb5" async="async"></script>
</body>
</html>
