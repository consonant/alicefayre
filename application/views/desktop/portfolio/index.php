<?php
$menu = config_item('menu-portfolio');
unset($menu[$category]);
?>

<div class="container picturebox" id="portfolio-list">
	
	<div class="row">
		<?php foreach($items as $slug => $item) : ?>
			<?php
				$cover = 'assets/img/'.$section.'/'.$category.'/'.$slug.'/0.jpg';
				if(ENVIRONMENT == 'development' && !file_exists($cover)) {
					$cover = 'assets/img/not_found.png';
				}
			?>

			<div class="lightgallery col-sm-6 portfolio-item">
				<?php for($i = 1; $i <= $item['gallery_n']; $i++) : ?>
					<a href="<?php echo base_url('assets/img/portfolio/'.$category.'/'.$slug.'/'.$i.'.jpg') ?>" data-src="<?php echo base_url('assets/img/portfolio/'.$category.'/'.$slug.'/'.$i.'.jpg') ?>">
						<?php if($i == 1) : ?>
							<div class="col-sm-6 col-md-5 col-lg-4 text middle-h-container hidden-xs">
								<div class="text-container middle-h">
									<p class="uppercase small list-name"><?php echo $item['name'] ?></p>
									<h4 class="list-client"><?php echo $item['client'] ?></h4>
									<br>
									<p class="small"><?php print_r($item['note']) ?></p>
									<p class="small label label-default"><?php echo $item['tags'] ?></p>
								</div>
							</div>

							<div class="col-sm-6 col-md-7 col-lg-8 img" style="background-image: url(<?php echo base_url($cover) ?>);">
								<div class="text-sm visible-xs">
									<p class="uppercase small list-name"><?php echo $item['name'] ?></p>
									<h4 class="list-client"><?php echo $item['client'] ?></h4>
									<span class="small"><?php print_r($item['note']) ?></span>
								</div>
							</div>
						<?php endif ?>
					</a>
				<?php endfor ?>
			</div>
		<?php endforeach ?>	
	</div>
</div>

<div id="more-categories-container">
	<div class="container">
		<h3><?php echo lang('more-categories') ?></h3>
		<div class="row">
			<?php foreach ($menu as $key => $value) : ?>
				<div class="col-sm-6">
					<a id="id-<?php echo($key) ?>" href="<?php echo site_url($value) ?>">
						<h1 class="categories-link"><?php echo lang('menu.'.$key) ?>.</h1>
					</a>
				</div>
			<?php endforeach ?>
		</div>
	</div>
</div>