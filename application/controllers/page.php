<?php

class Page extends MY_Controller {

	public function view($page = 'home')
	{
		// util
		$this->view_data['title'] = lang('title-'.$page) ? lang('title-'.$page) : ucfirst($page); // Capitalize the first letter
		$this->view_data['section'] = $page;

		$this->view_data['section_title'] = lang('title-'.$page);

		// seo
		$this->view_data['meta_description'] = lang('meta-description-'.$page);
		$this->view_data['meta_keywords'] = lang('meta-keywords-'.$page);

		// view
		$this->view_data['view'] = 'pages/' . $page;
	}
}