<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contacts extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->view_data['form_send_ok'] = FALSE;

		$this->load->library('form_validation');
		$this->load->library('user_agent');

		//var_dump($_POST);
		//exit();
	}

	// send
	public function send()
	{

		// field name, error message, validation rules
		$this->form_validation->set_rules('contactname', 'nome', 'trim|xss_clean|required');
		$this->form_validation->set_rules('contactemail', 'email', 'trim|xss_clean|required|valid_email');
		//$this->form_validation->set_rules('contactphone', 'telefono', 'trim|xss_clean|required');
    	$this->form_validation->set_rules('contactmessage', 'messaggio', 'trim|xss_clean|encode_php_tags|required|min_length[2]|max_length[900]');

		if($this->form_validation->run() == FALSE)
		{
			$this->session->set_userdata('form', $this->input->post());
			$this->session->set_flashdata('contactformerror', validation_errors());
			redirect('about#contactformcontainer');
		}
		else
		{
			// validation has passed. Now send the email
			$name = $this->input->post('contactname');
			$email = $this->input->post('contactemail');
			//$phone = $this->input->post('contactphone');
      		$message = $this->input->post('contactmessage');

			$this->load->library('email');
			$this->email->set_newline("\r\n");

			$this->email->from($email, $name);
			$this->email->to($this->config->item('contactmail'));
			$this->email->subject('['.$this->config->item('appname').'] Messaggio da area Contatti');
			$this->email->message($message);

			//$path = $this->config->item('server_root');
			//$file = $path . '/ci_day4/attachments/newsletter1.txt';
			//$this->email->attach($file);

			if($this->email->send())
			{
				//echo 'Your email was sent, fool.';
				$this->session->set_userdata('form', false);
				$this->session->set_flashdata('message', 'Il messaggio &egrave; stato spedito correttamente.');
				redirect($this->lang->lang() . '/about');
			}

			else
			{
				show_error($this->email->print_debugger());
			}
		}
	}


}

/* End of file about.php */
/* Location: ./application/controllers/about.php */
