<?php

class Portfolio extends MY_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function index($category)
	{
		//$this->view_data['section_title'] = lang('title-exhibitions');

		// util
		$this->view_data['title'] = lang('title-'.$category);
		$this->view_data['section'] = 'portfolio';

		// seo
		$this->view_data['meta_description'] = lang('meta-description-portfolio-'.$category);
		$this->view_data['meta_keywords'] = lang('meta-keywords-portfolio-'.$category);
		
		$this->view_data['category'] = $category;

		$portfolio = config_item('portfolio');

		// category
		$unordered_items = $portfolio[$category];
		$this->view_data['cose'] = $unordered_items;

		// sort items by date
		$items = array_orderby($unordered_items, 'date', SORT_DESC);


		$this->view_data['items'] = $items;

		/***
		* debug and scaffolding
		***/
		if(ENVIRONMENT == 'development') {

			// view elements directory
			$section = $this->view_data['section'];
			$assets_dir = dirname(__FILE__).'/../../assets'; 
			$img_dir = $assets_dir.'/img/'.$section.'/'.$category;
			//$video_dir = $assets_dir.'/video/'.$section;

			// create img directory
			if(! is_dir($img_dir)) {
				mkdir($img_dir, 0777, TRUE);
			}

			// create video directory
			//if(! is_dir($video_dir)) {
			//	mkdir($video_dir, 0777, TRUE);
			//}

			// list
			foreach ($items as $slug => $item) {
				//print_r2($item);

				// view element
				$element = $slug;

				// item img dir
				if(! is_dir($img_dir.'/'.$element)) {
					mkdir($img_dir.'/'.$element, 0777, TRUE);
				}

				// item video dir
				//if(isset($item['videos']) && is_array($item['videos'])) {
				//	if(! is_dir($video_dir.'/'.$element)) {
				//		mkdir($video_dir.'/'.$element, 0777, TRUE);
				//	}	
				//}
			}
		}
		/*** end debug and scaffolding ***/
	}	
}