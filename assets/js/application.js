/** UTILS ************************************************************************/
// timewatch
var timewatch = (function(){
  var timer = 0;
  return function(callback, ms){
    clearTimeout (timer);
    timer = setTimeout(callback, ms);
  }  
})();

// Scroll
$(function() {
  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});

// capitalize
String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

// url parameters
function getUrlParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
}

// resize 
$(window).on('resize', function() {
    timewatch(function() {
        console.count('resized - timewatched')
        
        var wh = $(window).innerHeight();
        var ww = $(window).innerWidth();

        $(window).trigger('app:resize', { 
            wh: wh,
            ww: ww 
        })

    }, 300)
    
    //console.count('resized')
})

// crossbrowser transition name (using modernizr)
var endTransitionName = {
    'WebkitTransition'	: 'webkitTransitionEnd',
    'OTransition'		: 'oTransitionEnd',
    'msTransition'		: 'MSTransitionEnd',
    'transition'		: 'transitionend'
};
var transitionEventName = endTransitionName[ Modernizr.prefixed( 'transition' ) ];

/********************************************************************************/

APP = {};

// Fast Click
/***
$(function() {
	FastClick.attach(document.body);
});
/***/

$(window).on('app:resize', function(e, dim) {
	console.log(e.type, dim)
})



$('.scroll').on('click', function(e) {
    e.preventDefault();

    var targetOffset = $($(this).attr('href')).offset();
    var menuHeight = $('#main-menu').height();

    var delay = 800;
    
    $("html, body").animate({ scrollTop: targetOffset.top - menuHeight }, delay, 'swing');
})


/** UNVEIL **/
/*$(".unveil img").unveil(200, function() {
  $(this).load(function() {
    this.style.opacity = 1;
  });
});*/

$(document).ready(function() {
    $(".lightgallery").lightGallery(); 
});



// MENU
jQuery(document).ready(function($){
  //if you change this breakpoint in the style.css file (or _layout.scss if you use SASS), don't forget to update this value as well
  var MQL = 1170;

  //primary navigation slide-in effect
  if($(window).width() > MQL) {
    var headerHeight = $('.cd-header').height();
    $(window).on('scroll',
    {
          previousTop: 0
      }, 
      function () {
        var currentTop = $(window).scrollTop();
        //check if user is scrolling up
        if (currentTop < this.previousTop ) {
          //if scrolling up...
          if (currentTop > 0 && $('.cd-header').hasClass('is-fixed')) {
            $('.cd-header').addClass('is-visible');
          } else {
            $('.cd-header').removeClass('is-visible is-fixed');
          }
        } else {
          //if scrolling down...
          $('.cd-header').removeClass('is-visible');
          if( currentTop > headerHeight && !$('.cd-header').hasClass('is-fixed')) $('.cd-header').addClass('is-fixed');
        }
        this.previousTop = currentTop;
    });
  }

  //open/close primary navigation
  $('.cd-primary-nav-trigger').on('click', function(){
    $('.cd-menu-icon').toggleClass('is-clicked'); 
    $('.cd-header').toggleClass('menu-is-open');
    
    //in firefox transitions break when parent overflow is changed, so we need to wait for the end of the trasition to give the body an overflow hidden
    if( $('.cd-primary-nav').hasClass('is-visible') ) {
      $('.cd-primary-nav').removeClass('is-visible').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend',function(){
        $('body').removeClass('overflow-hidden');
      });
    } else {
      $('.cd-primary-nav').addClass('is-visible').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend',function(){
        $('body').addClass('overflow-hidden');
      }); 
    }
  });
});