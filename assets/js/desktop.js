/*** IG *******************
var CLIENT_ID = '8dfcdc0b613c481b95cb3705136b3be7'; //'683b239c23f94d34bd99f59009b0b75d'; // app id
var USER_ID = '291471915'; //gebnegozionline // user id for currently displayed feed
var COUNT = 20; // n of images

$.fn.IG = function( options ) {

    var $element = this;

    var url = 'https://api.instagram.com/v1/users/'+USER_ID+'/media/recent/?count='+COUNT+'&client_id='+CLIENT_ID   

    var xhr = $.ajax({
        url: url,
        dataType: 'jsonp',
        success: function(res) {
            var $ul = $('<ul>');
            $ul.attr('id', 'ig');

            var loaded = 0;
            var nelements = $(res.data).length;

            $(res.data).each(function(idx, item) {
                var $li = $('<li class="">');
                $li.attr('id', 'item_' + idx);

                var $a = $('<a href="'+item.link+'" target="_blank">');
                $a.attr('id', item.id);
                
                var $img = $('<img src="/assets/img/ig/imgloader.png"/>');
                var src = item.images.standard_resolution.url;
                var $actualimg = $('<img src="'+src+'"/>');

                $a.append($img)
                $li.append($a)
                $ul.append($li)
                $element.append($ul)

                $actualimg.on('load', function() {
                    $img.fadeOut(200, function() {
                        $img.attr('src', src);
                        $img.fadeIn(200);
                    })
                    
                    loaded++;

                    if(loaded == nelements) {
                        // animate
                        $ul.slick({
                            infinite: true,
                            autoplay: true,
                            autoplaySpeed: 4000,
                            vertical: true,
                            slidesToShow: 4,
                            adaptiveHeight: true,
                            pauseOnHover: false
                        })
                    }
                })
            })
        }
    }); 
} // IG

// populate sidebar
$('#sidebar').IG();

/********************************************/