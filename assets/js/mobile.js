/**
 * 
 * Mobile panels
 * 
 */

// Click sul mobile-overlay per chiude i mobile panel
$('.mobile-overlay').on('click', function(){
    $(".mobile-panel, .mobile-overlay,body").removeClass("isOpen");
});


// Trigger il Mobile panel dell'icona cliccata.
$('.mobile-panel-trigger').on('click touchend', function(e){
    e.preventDefault();

    // Cache il selettore del pannello da triggare
    var triggedPanel = $("#" + "mobile-" + $(this).data("panel"));
    triggedPanel.toggleClass("isOpen").siblings('.mobile-panel').removeClass('isOpen');
    if( triggedPanel.hasClass("isOpen")) {
        $(".mobile-overlay,body").addClass("isOpen");
    } else {
        $(".mobile-overlay,body").removeClass("isOpen");
    }
});


// Triggare le sub-menu
$(".mobile-panel a").on("click",function(e){
    var next = $(this).next(".sub-level");
    if(next.length) {
        e.preventDefault();
        next.addClass('isOpen');
        $(this).parents('ul').css('overflow', 'hidden');
     }
});